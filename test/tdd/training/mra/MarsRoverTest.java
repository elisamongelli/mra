package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class MarsRoverTest {
	/*
	@Test
	public void testGetObstacleAt4_7() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertTrue(rover.planetContainsObstacleAt(4,7));
	}

	@Test (expected = MarsRoverException.class)
	public void testGetExceptionObstacleAt12_3() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(12,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
	}
	
	@Test
	public void testGetObstacleAt4_2() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertFalse(rover.planetContainsObstacleAt(4,2));
	}
	
	@Test
	public void testGetPositionWithEmptyCommandString() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testGetPositionWithTurningRightCommand() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testGetPositionWithTurningLeftCommand() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testGetPositionWithMovingForwardCommand() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertEquals("(0,1,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testGetPositionWithMovingBackwardCommand() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		rover.setCurrentPosition("(5,8,E)");
		
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void testGetPositionWithCombinedMoving() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testGetPositionWithSphericalPlanet() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	*/
	@Test
	public void testGetPositionWithSingleObstacle() throws MarsRoverException {
		ArrayList<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(10, 10, obstacles);
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
	}
}
