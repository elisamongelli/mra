package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private int planetX;
	private int planetY;
	List<String> planetObstacles = new ArrayList<String>();
	
	private String landingPosition;
	private StringBuilder currentPosition;
	private String positions = "NESW";

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		
		if (!obstaclesAreValid(planetObstacles))
			throw new MarsRoverException("Invalid value for an obstacle");
		this.planetObstacles = planetObstacles;
		this.landingPosition = "(0,0,N)";
		this.currentPosition = new StringBuilder("(0,0,N)");
	}
	
	public void setCurrentPosition(String currentPosition) throws MarsRoverException {
		this.currentPosition = new StringBuilder(currentPosition);
	}
	
	public boolean obstaclesAreValid(List<String> obstacles) throws MarsRoverException {
		int obstacle;
		
		for(int i = 0; i < obstacles.size(); i++) {
			obstacle = Character.getNumericValue(obstacles.get(i).charAt(1));
			if(obstacle < 0 || obstacle >= this.planetX) {
				return false;
			}
		}
		for(int i = 0; i < obstacles.size(); i++) {
			obstacle = Character.getNumericValue(obstacles.get(i).charAt(3));
			if(obstacle < 0 || obstacle >= this.planetY) {
				return false;
			}
		}
		return true;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		String obstacle = "(" + x + "," + y + ")";
		if(this.planetObstacles.contains(obstacle))
			return true;
		else return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		StringBuilder tempPosition = new StringBuilder(currentPosition);
		int positionIndex = positions.indexOf(currentPosition.charAt(5));
		char currentCommand;
		
		ArrayList<String> encounteredObstacle = new ArrayList<String>();
		String currentObstacle = "";
		
		
		if (commandString == "")
			return this.landingPosition;
		
		
		for (int i = 0; i < commandString.length(); i++) {
			currentCommand = commandString.charAt(i);
			
			if (currentCommand == 'r')
				if (positionIndex != positions.length()-1)
					currentPosition.setCharAt(5, positions.charAt(positionIndex+1));
				else currentPosition.setCharAt(5, positions.charAt(0));
			
			else if (currentCommand == 'l')
				if (positionIndex != 0)
					currentPosition.setCharAt(5, positions.charAt(positionIndex-1));
				else currentPosition.setCharAt(5, positions.charAt(positions.length()-1));
			
			
			else if (currentCommand == 'f') {
				
				if (currentPosition.charAt(5) == 'N' && currentPosition.charAt(3) == Character.forDigit(planetY-1, 10)) {
					tempPosition.setCharAt(3, '0');
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'N' && Character.getNumericValue(currentPosition.charAt(3)) < planetY-1) {
					tempPosition.setCharAt(3, Character.forDigit(Character.getNumericValue(currentPosition.charAt(3))+1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
				
				if (currentPosition.charAt(5) == 'E' && currentPosition.charAt(1) == Character.forDigit(planetX-1, 10)){
					tempPosition.setCharAt(1, '0');
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'E' && Character.getNumericValue(currentPosition.charAt(1)) < planetX-1) {
					tempPosition.setCharAt(1, Character.forDigit(Character.getNumericValue(currentPosition.charAt(1))+1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
				
				if (currentPosition.charAt(5) == 'S' && currentPosition.charAt(3) == '0') {
					tempPosition.setCharAt(3, Character.forDigit(planetY-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'S' && Character.getNumericValue(currentPosition.charAt(3)) > 0) {
					tempPosition.setCharAt(3, Character.forDigit(Character.getNumericValue(currentPosition.charAt(3))-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
				
				if (currentPosition.charAt(5) == 'W' && currentPosition.charAt(1) == '0') {
					tempPosition.setCharAt(1, Character.forDigit(planetX-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'W' && Character.getNumericValue(currentPosition.charAt(1)) > 0){
					tempPosition.setCharAt(1, Character.forDigit(Character.getNumericValue(currentPosition.charAt(1))-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = new StringBuilder(tempPosition);
						continue;
					}
				}
			}
			
			else if (currentCommand == 'b') {
				
				if (currentPosition.charAt(5) == 'N' && currentPosition.charAt(3) == '0') {
					tempPosition.setCharAt(3, Character.forDigit(planetY-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'N' && Character.getNumericValue(currentPosition.charAt(3)) > 0){
					tempPosition.setCharAt(3, Character.forDigit(Character.getNumericValue(currentPosition.charAt(3))-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
				
				if (currentPosition.charAt(5) == 'E' && currentPosition.charAt(1) == '0') {
					tempPosition.setCharAt(1, Character.forDigit(planetX-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'E' && Character.getNumericValue(currentPosition.charAt(1)) > 0) {
					tempPosition.setCharAt(1, Character.forDigit(Character.getNumericValue(currentPosition.charAt(1))-1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
				
				if (currentPosition.charAt(5) == 'S' && currentPosition.charAt(3) == Character.forDigit(planetY-1, 10)){
					tempPosition.setCharAt(3, '0');
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'S' && Character.getNumericValue(currentPosition.charAt(3)) < planetY-1) {
					tempPosition.setCharAt(3, Character.forDigit(Character.getNumericValue(currentPosition.charAt(3))+1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
				
				if (currentPosition.charAt(5) == 'W' && currentPosition.charAt(1) == Character.forDigit(planetX-1, 10)){
					tempPosition.setCharAt(1, '0');
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
				else if (currentPosition.charAt(5) == 'W' && Character.getNumericValue(currentPosition.charAt(1)) < planetX-1){
					tempPosition.setCharAt(1, Character.forDigit(Character.getNumericValue(currentPosition.charAt(1))+1, 10));
					if (planetContainsObstacleAt(Character.getNumericValue(tempPosition.charAt(1)), Character.getNumericValue(tempPosition.charAt(3)))) {
						currentObstacle = "(" + tempPosition.charAt(1) + "," + tempPosition.charAt(3) + ")";
						if (!encounteredObstacle.contains(currentObstacle)) {
							encounteredObstacle.add(currentObstacle);
							continue;
						}
						continue;
					}
					else {
						currentPosition = tempPosition;
						continue;
					}
				}
			}
		}
		
		return currentPosition.toString() + String.join("", encounteredObstacle);
	}

}
